const form = document.getElementById("myform");
const status = document.getElementById("status");

form.addEventListener("submit", async (e) => {
  e.preventDefault();
  const formData = new FormData(form);
  try {
    const response = await fetch(form.action, {
      method: form.method,
      body: formData,
      headers: {
        Accept: "application/json",
      },
    });
    form.reset();
    status.textContent = "Thank you! Your message has been sent.";
    status.style.color = "green";
  } catch (e) {
    status.textContent = "Oops! There was a problem submitting your form.";
  }
});